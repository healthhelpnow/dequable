import UIKit

public protocol DequeableCollectionView: Dequeable {
    func register(headerFooterViewType: DequeableComponentIdentifiable.Type, forSupplementaryViewOfKind kind: String, hasXib: Bool)
    func dequeueCell<T>(at indexPath: IndexPath) -> T where T : UICollectionViewCell & DequeableComponentIdentifiable
    func dequeueSupplementaryView<T>(at indexPath: IndexPath, ofKind kind: String) -> T where T : UICollectionReusableView & DequeableComponentIdentifiable
}

public extension DequeableCollectionView where Self: UICollectionView {
    
    func register(headerFooterViewType: DequeableComponentIdentifiable.Type, forSupplementaryViewOfKind kind: String, hasXib: Bool) {
        let identifier = headerFooterViewType.dequableComponentIdentifier
        if hasXib == true {
            let className = typeName(headerFooterViewType)
            let nib = UINib(nibName: className, bundle: Bundle(for: headerFooterViewType))
            register(nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: identifier)
        } else {
            register(headerFooterViewType, forSupplementaryViewOfKind: kind, withReuseIdentifier: identifier)
        }
    }
    
    func dequeueCell<T>(at indexPath: IndexPath) -> T where T : UICollectionViewCell & DequeableComponentIdentifiable {
      return dequeueReusableCell(withReuseIdentifier: T.dequableComponentIdentifier, for: indexPath) as! T
    }
  
    func dequeueSupplementaryView<T>(at indexPath: IndexPath, ofKind kind: String) -> T where T : UICollectionReusableView & DequeableComponentIdentifiable {
        return dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: T.dequableComponentIdentifier, for: indexPath) as! T
    }
}
