import UIKit

public protocol DequeableTableView: Dequeable {
    func register(headerFooterViewType: DequeableComponentIdentifiable.Type, hasXib: Bool)
    func dequeueCell<T>(at indexPath: IndexPath) -> T where T : UITableViewCell & DequeableComponentIdentifiable
    func dequeueHeaderFooterView<T>() -> T where T : UITableViewHeaderFooterView & DequeableComponentIdentifiable
}

public extension DequeableTableView where Self: UITableView {
    
    func register(headerFooterViewType: DequeableComponentIdentifiable.Type, hasXib: Bool) {
        let identifier = headerFooterViewType.dequableComponentIdentifier
        if hasXib == true {
            let className = typeName(headerFooterViewType)
            let bundle = Bundle(for: headerFooterViewType)
            let nib = UINib(nibName: className, bundle: bundle)
            register(nib, forHeaderFooterViewReuseIdentifier: identifier)
        } else {
            register(headerFooterViewType, forHeaderFooterViewReuseIdentifier: identifier)
        }
    }
    
    func dequeueCell<T>(at indexPath: IndexPath) -> T where T : UITableViewCell & DequeableComponentIdentifiable {
        return dequeueReusableCell(withIdentifier: T.dequableComponentIdentifier, for: indexPath) as! T
    }
    
    func dequeueHeaderFooterView<T>() -> T where T : UITableViewHeaderFooterView & DequeableComponentIdentifiable {
        return dequeueReusableHeaderFooterView(withIdentifier: T.dequableComponentIdentifier) as! T
    }
}
