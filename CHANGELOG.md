# Change Log

## [3.0.0](https://github.com/rob-nash/Dequable/releases/tag/3.0.0) (2019-Jan-21)

 - A default value for hasXib is no longer provided.
 - Some public API has been slightly rephrased.

## [2.0.1](https://github.com/rob-nash/Dequable/releases/tag/2.0.1) (2018-Nov-09)

 - Swift 4.2.1

## [2.0.0](https://github.com/rob-nash/Dequable/releases/tag/2.0.0) (2018-Sep-17)

 - Swift 4.2

## [1.4.1](https://github.com/rob-nash/Dequable/releases/tag/1.4.1) (2018-Aug-16)

 - Handle registration and dequeue of UITableViewHeaderFooterView

## [1.3.1](https://github.com/rob-nash/Dequable/releases/tag/1.3.1) (2018-Aug-09)

 - Refactoring. Instead of NSStringFromClass 👇

```swift
func typeName(_ some: Any) -> String {
    return (some is Any.Type) ? "\(some)" : "\(type(of: some))"
}
```

## [1.3.0](https://github.com/rob-nash/Dequable/releases/tag/1.3.0) (2018-Jan-24)

 - Adding dequeueReusableSupplementaryView

## [1.2.0](https://github.com/rob-nash/Dequable/releases/tag/1.2.0) (2018-Jan-24)

 - hasNib changed to hasXib
 - hasXib is false by default
 - demo is simpler implementation for clarity.

## [1.1.0](https://github.com/rob-nash/Dequable/releases/tag/1.1.0) (2017-Dec-05)

 - Dequeuing cells for collection views now has similar syntax to that of table view counterpart.

## [1.0.0](https://github.com/rob-nash/Dequable/releases/tag/1.0.0) (2017-Nov-09)

 - First release
