import UIKit
import Dequable

final class ItemTableViewCell: UITableViewCell, DequeableComponentIdentifiable {}
