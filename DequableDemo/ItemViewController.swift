import UIKit

final class ItemViewController: UIViewController {
    
    private let tableViewHandler: ItemTableViewHandler
    
    init(tableViewHandler: ItemTableViewHandler) {
        
        let tableView = ItemTableView()
        tableView.dataSource = tableViewHandler
        tableViewHandler.delegate = tableView
        self.tableViewHandler = tableViewHandler
        
        super.init(nibName: nil, bundle: nil)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let name = String(4)
            let category = ContentBuilder.makeCategory(name: name)
            self.tableViewHandler.insert(category: category)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let name = String(3)
            let category = ContentBuilder.makeCategory(name: name)
            self.tableViewHandler.insert(category: category)
        }
    }
}
