import UIKit

protocol ItemTableViewHandlerDelegate: class {
    func handler(_ handler: ItemTableViewHandler, didInsertCategory category: Category, atIndex indexSet: IndexSet)
    func handler(_ handler: ItemTableViewHandler, didRemoveCategory category: Category, atIndex indexSet: IndexSet)
    func handler(_ handler: ItemTableViewHandler, didInsertItem item: Item, atIndex indexPath: IndexPath)
    func handler(_ handler: ItemTableViewHandler, didRemoveItem item: Item, atIndex indexPath: IndexPath)
}

extension ItemTableViewHandlerDelegate where Self: UITableView {
    
    func handler(_ handler: ItemTableViewHandler, didInsertCategory category: Category, atIndex indexSet: IndexSet) {
        insertSections(indexSet, with: .automatic)
    }
    
    func handler(_ handler: ItemTableViewHandler, didRemoveCategory category: Category, atIndex indexSet: IndexSet) {
        deleteSections(indexSet, with: .automatic)
    }
    
    func handler(_ handler: ItemTableViewHandler, didInsertItem item: Item, atIndex indexPath: IndexPath) {
        insertRows(at: [indexPath], with: .automatic)
    }
    
    func handler(_ handler: ItemTableViewHandler, didRemoveItem item: Item, atIndex indexPath: IndexPath) {
        deleteRows(at: [indexPath], with: .automatic)
    }
}
