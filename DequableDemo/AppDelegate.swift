import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = makeRoot()
        window?.makeKeyAndVisible()
        return true
    }
    
    private func makeRoot() -> UINavigationController {
        let categories = ContentBuilder.makeCategories()
        let tableViewHandler = ItemTableViewHandler(categories: SortedArray(sorted: categories))
        let controller = ItemViewController(tableViewHandler: tableViewHandler)
        return UINavigationController(rootViewController: controller)
    }
}
