import Foundation

final class Item: Base {
    
    unowned let category: Category
    
    init(value: Int, category: Category) {
        self.category = category
        super.init(value: value)
    }
    
    static func ==(lhs: Item, rhs: Item) -> Bool {
        return lhs.name == rhs.name
    }
    
    static func <(lhs: Item, rhs: Item) -> Bool {
        return lhs.index < rhs.index
    }
}
