import Foundation

final class Category: Base {
    
    var items: SortedArray<Item> = SortedArray(sorted: [])
    
    static func ==(lhs: Category, rhs: Category) -> Bool {
        return lhs.name == rhs.name
    }
    
    static func <(lhs: Category, rhs: Category) -> Bool {
        return lhs.index < rhs.index
    }
}
