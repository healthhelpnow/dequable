import Foundation

class Base {
    
    let name: String
    
    var index: Int {
        return Int(name)!
    }
    
    init(name: String) {
        self.name = name
    }
    
    init(value: Int) {
        self.name = String(value)
    }
}

extension Base: Comparable {
    
    static func ==(lhs: Base, rhs: Base) -> Bool {
        return lhs.name == rhs.name
    }
    
    static func <(lhs: Base, rhs: Base) -> Bool {
        return lhs.index < rhs.index
    }
}
