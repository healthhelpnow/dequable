import Foundation

struct ContentBuilder {
    
    static func makeCategories() -> SortedArray<Category> {
        let categories: [Category] = (1...2).map {
            let name = String($0)
            return makeCategory(name: name)
        }
        return SortedArray<Category>(sorted: categories)
    }
    
    static func makeCategory(name: String) -> Category {
        let category = Category(name: name)
        let value = Int.random(in: 1...4)
        let items = (1...value).map {
            Item(value: $0, category: category)
        }
        category.items = SortedArray(sorted: items)
        return category
    }
}
