import Foundation

struct ItemTableViewCellConfigurator<Model> {
    
    let titleKeyPath: KeyPath<Model, String>
    
    func configure(cell: ItemTableViewCell, model: Model) {
        cell.textLabel?.text = model[keyPath: titleKeyPath]
    }
}
