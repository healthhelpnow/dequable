import UIKit
import Dequable

final class ItemTableViewHandler: NSObject {
    
    weak var delegate: ItemTableViewHandlerDelegate?
    
    private var categories: SortedArray<Category>
    
    init(categories: SortedArray<Category>) {
        self.categories = categories
    }
    
    func insert(category: Category) {
        guard categories.contains(category) == false else {
            return
        }
        let section = categories.insert(category)
        let indexSet = IndexSet([section])
        delegate?.handler(self, didInsertCategory: category, atIndex: indexSet)
    }
    
    func insert(item: Item) {
        if let section = categories.firstIndex(of: item.category) {
            let category = categories[section]
            guard category.items.contains(item) == false else {
                return
            }
            let row = category.items.insert(item)
            let indexPath = IndexPath(row: row, section: section)
            delegate?.handler(self, didInsertItem: item, atIndex: indexPath)
        } else {
            insert(category: item.category)
        }
    }
    
    func remove(category: Category) {
        if let section = categories.remove(category) {
            let indexSet = IndexSet([section])
            delegate?.handler(self, didRemoveCategory: category, atIndex: indexSet)
        }
    }
    
    func remove(item: Item) {
        if let section = categories.firstIndex(of: item.category) {
            let category = categories[section]
            if let row = category.items.remove(item) {
                let indexPath = IndexPath(row: row, section: section)
                delegate?.handler(self, didRemoveItem: item, atIndex: indexPath)
            }
        }
    }
}

extension ItemTableViewHandler: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let category = categories[section]
        let items = category.items
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let dequableTableView = tableView as? DequeableTableView else {
            fatalError("Must conform to DequeableTableView")
        }
        let category = categories[indexPath.section]
        let items = category.items
        let item: Item = items[indexPath.row]
        let cell: ItemTableViewCell = dequableTableView.dequeueCell(at: indexPath)
        let configurator = ItemTableViewCellConfigurator<ItemViewModel>(titleKeyPath: \.title)
        let itemViewModel = ItemViewModel(item)
        configurator.configure(cell: cell, model: itemViewModel)
        return cell
    }
}

extension ItemTableViewHandler: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let category = categories[section]
        return category.name
    }
}
