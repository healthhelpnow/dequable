import UIKit
import Dequable

final class ItemTableView: UITableView, DequeableTableView, ItemTableViewHandlerDelegate {
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        register(cellType: ItemTableViewCell.self, hasXib: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
